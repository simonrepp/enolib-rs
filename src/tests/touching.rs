use indoc::formatdoc;

use crate::prelude::*;
use crate::parse;

#[test]
fn test_untouched_elements_attribute() {
    let document = parse(&formatdoc!(r#"
        field:
        attribute =
    "#)).unwrap();
    
    let untouched_elements = document.untouched_elements();
    
    assert_eq!(untouched_elements.len(), 1);
    assert_eq!(untouched_elements[0].line_number(), 1);
    
    let field = document.field("field").unwrap();
    field.touch();
    
    let untouched_elements = document.untouched_elements();
    
    assert_eq!(untouched_elements.len(), 1);
    assert_eq!(untouched_elements[0].line_number(), 2);
    
    let attribute = field.attribute("attribute").unwrap();
    attribute.touch();
    
    assert!(document.untouched_elements().is_empty());
}

#[test]
fn test_untouched_elements_elements() {
    let document = parse(&formatdoc!(r#"
        flag1
        flag2
    "#)).unwrap();

    let untouched_elements = document.untouched_elements();
    
    assert_eq!(untouched_elements.len(), 2);
    assert_eq!(untouched_elements[0].line_number(), 1);
    assert_eq!(untouched_elements[1].line_number(), 2);
    
    for element in document.elements() {
        element.touch();
    }
    
    assert!(document.untouched_elements().is_empty());
}

#[test]
fn test_untouched_elements_embed() {
    let document = parse(&formatdoc!(r#"
        -- embed
        -- embed
    "#)).unwrap();
    
    let untouched_elements = document.untouched_elements();
    
    assert_eq!(untouched_elements.len(), 1);
    assert_eq!(untouched_elements[0].line_number(), 1);
    
    let embed = document.embed("embed").unwrap();
    embed.touch();
    
    assert!(document.untouched_elements().is_empty());
}

#[test]
fn test_untouched_elements_field() {
    let document = parse("field:").unwrap();
    
    let untouched_elements = document.untouched_elements();
    
    assert_eq!(untouched_elements.len(), 1);
    assert_eq!(untouched_elements[0].line_number(), 1);
    
    let field = document.field("field").unwrap();
    field.touch();
    
    assert!(document.untouched_elements().is_empty());
}

#[test]
fn test_untouched_elements_flag() {
    let document = parse("flag").unwrap();
    
    let untouched_elements = document.untouched_elements();
    
    assert_eq!(untouched_elements.len(), 1);
    assert_eq!(untouched_elements[0].line_number(), 1);
    
    let flag = document.flag("flag").unwrap();
    flag.touch();
    
    assert!(document.untouched_elements().is_empty());
}

#[test]
fn test_untouched_elements_item() {
    let document = parse(&formatdoc!(r#"
        field:
        -
    "#)).unwrap();

    let untouched_elements = document.untouched_elements();
    
    assert_eq!(untouched_elements.len(), 1);
    assert_eq!(untouched_elements[0].line_number(), 1);
    
    let field = document.field("field").unwrap();
    field.touch();
    
    let untouched_elements = document.untouched_elements();
    
    assert_eq!(untouched_elements.len(), 1);
    assert_eq!(untouched_elements[0].line_number(), 2);
    
    for item in field.items().unwrap() {
        item.touch();
    }
    
    assert!(document.untouched_elements().is_empty());
}

#[test]
fn test_untouched_elements_section() {
    let document = parse("# section").unwrap();
    
    let untouched_elements = document.untouched_elements();
    
    assert_eq!(untouched_elements.len(), 1);
    assert_eq!(untouched_elements[0].line_number(), 1);
    
    let section = document.section("section").unwrap();
    section.touch();
    
    assert!(document.untouched_elements().is_empty());
}

#[test]
fn test_untouched_elements_section_flag() {
    let document = parse(&formatdoc!(r#"
        # section
        flag
    "#)).unwrap();

    let untouched_elements = document.untouched_elements();
    
    assert_eq!(untouched_elements.len(), 1);
    assert_eq!(untouched_elements[0].line_number(), 1);
    
    let section = document.section("section").unwrap();
    section.touch();
    
    let untouched_elements = document.untouched_elements();
    
    assert_eq!(untouched_elements.len(), 1);
    assert_eq!(untouched_elements[0].line_number(), 2);
    
    let flag = section.flag("flag").unwrap();
    flag.touch();
    
    assert!(document.untouched_elements().is_empty());
}

