use indoc::formatdoc;

use crate::prelude::*;
use crate::parse;


#[test]
fn test_touch_recursive_field() {
    let document = parse(&formatdoc!("
        field:
    ")).unwrap();

    document.field("field").unwrap().touch_recursive();

    assert!(document.untouched_elements().is_empty());
}

#[test]
fn test_touch_recursive_field_attributes() {
    let document = parse(&formatdoc!("
        field:
        attribute = value
        attribute = value
    ")).unwrap();

    document.field("field").unwrap().touch_recursive();

    assert!(document.untouched_elements().is_empty());
}

#[test]
fn test_touch_recursive_field_items() {
    let document = parse(&formatdoc!("
        field:
        - item
        - item
    ")).unwrap();

    document.field("field").unwrap().touch_recursive();

    assert!(document.untouched_elements().is_empty());
}

#[test]
fn test_touch_recursive_field_value() {
    let document = parse(&formatdoc!("
        field: value
    ")).unwrap();

    document.field("field").unwrap().touch_recursive();

    assert!(document.untouched_elements().is_empty());
}
